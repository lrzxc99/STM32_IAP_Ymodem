/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Uart.c
* 内容简述: 串口驱动层
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.4.15   candy     创建文件
* v0.2    2014.4.23   candy     串口全局配置
*******************************************************************************/
#include "OD_Uart.h"
#include <string.h>

/*******************************************************************************
*  函数名： od_UartPortConfig
*  输  入:  USARTx： 串口端口
*  输  出:  无
*  功能说明： 串口端口配置
* PA9=TXD1, PA10=RXD1; PA2=TXD2, PA3=RXD2;
*******************************************************************************/
static void od_UartPortConfig(USART_TypeDef* USARTx)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  if (USARTx == USART1)
  {
#if OD_UART1_USED
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);    /*开启GPIO的时钟*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);   /*开启串口的时钟*/
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;               /*串口的TX发送复用功能*/
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10;               /*串口的RX接受功能*/
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
#endif /*OD_UART1_USED*/
  }
  else if (USARTx == USART2)
  {
#if OD_UART2_USED
  #if OD_UART2_485
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_1;  /*485_EN*/
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
  #endif /*OD_UART2_485*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_3;  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
#endif /*OD_UART2_USED*/
  }
  else if (USARTx == USART3)
  {
#if OD_UART3_USED
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10;  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11;  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
#endif /*OD_UART3_USED*/
  }
  else
  {
    return;
  }
}

/*******************************************************************************
*  函数名： od_UartModeConfig
*  输  入:  无
*  输  出:  无
*  功能说明： 串口配置程序
*******************************************************************************/
static void od_UartModeConfig(USART_TypeDef* USARTx, USART_InitTypeDef USART_InitStructure)
{
  USART_Cmd(USARTx, DISABLE);
  USART_Init(USARTx, &USART_InitStructure);
  USART_ClearFlag(USARTx, USART_FLAG_TC);
  USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
  USART_Cmd(USARTx, ENABLE);
}



/*******************************************************************************
*  函数名： OD_UartInit
*  输  入:  无
*  输  出:  无
*  功能说明： 外拼串口的初始化
*******************************************************************************/
void OD_UartInit(void)
{
  USART_InitTypeDef USART_InitStructure;
#if OD_UART1_USED                /*对串口初始化，串口的波特率在H文件中定义*/
  od_UartPortConfig(USART1);

  USART_InitStructure.USART_WordLength = OD_UART1_WORD_LEN;
  USART_InitStructure.USART_StopBits = OD_UART1_STOPBIT;
  USART_InitStructure.USART_Parity = OD_UART1_PARITY;  
  USART_InitStructure.USART_BaudRate = OD_UART1_BAUD;  
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  od_UartModeConfig(USART1, USART_InitStructure);
#endif /*OD_UART1_USED*/
#if OD_UART2_USED                /*同上*/
  
  od_UartPortConfig(USART2);

  USART_InitStructure.USART_WordLength = OD_UART2_WORD_LEN;
  USART_InitStructure.USART_StopBits = OD_UART2_STOPBIT;
  USART_InitStructure.USART_Parity = OD_UART2_PARITY;  
  USART_InitStructure.USART_BaudRate = OD_UART2_BAUD;  
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  od_UartModeConfig(USART2, USART_InitStructure);
#endif /*OD_UART2_USED*/
#if OD_UART3_USED                 /*同上*/
  od_UartPortConfig(USART3);

  USART_InitStructure.USART_WordLength = OD_UART3_WORD_LEN;
  USART_InitStructure.USART_StopBits = OD_UART3_STOPBIT;
  USART_InitStructure.USART_Parity = OD_UART3_PARITY;  
  USART_InitStructure.USART_BaudRate = OD_UART3_BAUD;  
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  od_UartModeConfig(USART3, USART_InitStructure);
#endif /*OD_UART2_USED*/

}


/*******************************************************************************
*  函数名： OD_UartWrite
*  输  入:  USARTx： 要发送的串口
           sdata：要发送的数据
           len：要发送数据的长度
*  输  出:  无
*  功能说明： 串口数据的发送
*******************************************************************************/
void OD_UartWrite(USART_TypeDef* USARTx, uint8_t *wdata, uint16_t len)
{
  register uint16_t i;
  
  i = len;
  while (i > 0)
  {
    while (USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
    USART_SendData(USARTx, *wdata++);
    i--;
  }
}

