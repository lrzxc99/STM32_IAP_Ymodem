/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Interrupt.c
* 内容简述: 中断服务程序
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.4.11  candy    修改文件格式
*
*******************************************************************************/
#include "OD_Interrupt.h"
#include "OD_Uart.h"
#include "OD_SystemTick.h"

volatile uint16_t Timer_1ms = 0;
volatile extern uint16_t TimerCount;
#define AIRCR_VECTKEY_MASK    ((uint32_t)0x05FA0000)

/*******************************************************************************
*  函数名： OD_InterruptInit
*  输  入:  无
*  输  出:  无
*  功能说明： 中断初始化
*******************************************************************************/
void OD_InterruptInit(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;	       /*定时器2中断*/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  
  NVIC_Init(&NVIC_InitStructure); 
}


/*******************************************************************************
*  函数名： NMI_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 控制卡主程序
*******************************************************************************/
void NMI_Handler(void)
{
}

/*******************************************************************************
*  函数名： HardFault_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 硬件错误中断
*******************************************************************************/
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */

  DEBUG_ERROR("Hard fault, sp=0x%08x\r\n", __get_PSP());

  while (1)
  {
    SCB->AIRCR = AIRCR_VECTKEY_MASK | (uint32_t)0x04;
  }
}

/*******************************************************************************
*  函数名： MemManage_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 内存管理中断
*******************************************************************************/
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */

  DEBUG_ERROR("Memory manage fault\r\n");
  while (1)
  {
  }
}

/*******************************************************************************
*  函数名： BusFault_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 总线故障中断
*******************************************************************************/
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  DEBUG_ERROR("Bus fault\r\n");
  while (1)
  {
  }
}

/*******************************************************************************
*  函数名： UsageFault_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 使用故障中断
*******************************************************************************/
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  DEBUG_ERROR("Usage fault\r\n");
  while (1)
  {
  }
}

/*******************************************************************************
*  函数名： SVC_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 软件中断
*******************************************************************************/
void SVC_Handler(void)
{
}

/*******************************************************************************
*  函数名： DebugMon_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 调试监控中断
*******************************************************************************/
void DebugMon_Handler(void)
{
  DEBUG_ERROR("Debug Monitor fault");
}

/*******************************************************************************
*  函数名： PendSV_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 系统服务请侵卸象
*******************************************************************************/
void PendSV_Handler(void)
{
  DEBUG_ERROR("Pend Service fault");
}

/*******************************************************************************
*  函数名： SysTick_Handler
*  输  入:  无
*  输  出:  无
*  功能说明： 系统节拍定时器中断
*******************************************************************************/
void SysTick_Handler(void)          //1ms
{
  if (Timer_1ms)
  {
    Timer_1ms--;
  }
  
}

/*******************************************************************************
*  函数名： TIM2_IRQHandler
*  输  入:  无
*  输  出:  无
*  功能说明： 定时器2中断
*******************************************************************************/
void TIM2_IRQHandler(void)             //100ms
{
  TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
  if (TimerCount)
  {
    TimerCount--;
  }
}

/*******************************************************************************
*  函数名： TIM3_IRQHandler
*  输  入:  无
*  输  出:  无
*  功能说明： 定时器3中断
*******************************************************************************/
void TIM3_IRQHandler(void)            //500ms
{
  /* Clear TIM3 update interrupt */
  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
   
}

/*******************************************************************************
*  函数名： EXTI4_IRQHandler
*  输  入:  无
*  输  出:  无
*  功能说明： 外部中断线路4
*******************************************************************************/
void EXTI4_IRQHandler(void)
{

}

/*******************************************************************************
*  函数名： EXTI9_5_IRQHandler
*  输  入:  无
*  输  出:  无
*  功能说明： 外部中断
*******************************************************************************/
void EXTI9_5_IRQHandler(void)
{
  
}
