/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Flash.c
* 内容简述: 内部Flash存储器的驱动程序
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.5.13  candy    创建文件
* 
*******************************************************************************/
#include "OD_Flash.h"
#include <string.h>

/*******************************************************************************
*  函数名： OD_FlashInit
*  输  入:  无
*  输  出:  无
*  功能说明： 初始化内部Flash存储器
*******************************************************************************/
void OD_FlashInit(void)
{
  DEBUG_LOG("Flash initialize ok\r\n");
}

/*******************************************************************************
*  函数名： OD_FlashErasePage
*  输  入:  page：擦鞋地址，len：长度
*  输  出:  操作状态
*  功能说明： 擦除页
*******************************************************************************/
uint8_t OD_FlashErasePage(uint32_t page, uint16_t len)
{
  register uint16_t i;
  uint32_t page_temp;
  FLASH_Status status;
  page_temp = page + OD_FLASH_ADDR_BASE;
  page_temp = (page_temp / OD_FLASH_PAGE_SIZE) * OD_FLASH_PAGE_SIZE;  /*整页的擦除*/
  
  FLASH_Unlock();
  FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
  
  for (i = 0; i < len; i++)
  {
    status = FLASH_ErasePage(page_temp);
    if ((status == FLASH_ERROR_PG) || (status == FLASH_ERROR_WRP)) /*判断擦除是否正确*/
    {
      DEBUG_WARNING("Flash erase error, error code is 0x%02x", status);
      return 1;
    }
    page_temp += OD_FLASH_PAGE_SIZE;
  }
  
  FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
  
  FLASH_Lock();
  return 0;
}


/*******************************************************************************
*  函数名： OD_FlashRead
*  输  入:  addr：地址，dat：读取数据，len：长度
*  输  出:  无
*  功能说明： 读取Flash中的数据
*******************************************************************************/
void OD_FlashRead(uint32_t addr, uint8_t *dat, uint16_t len)
{
  register uint16_t i;
  uint32_t addr_temp;
  if (addr >= OD_FLASH_MAX_SIZE)
  {
    DEBUG_WARNING("Flash addr is over the max size\r\n");
    return;
  }
  addr_temp = addr + OD_FLASH_ADDR_BASE;
  
  for (i = 0; i < len; i++)
  {
    *(dat + i) = *(__IO uint8_t*)(addr_temp + i);
  }
}

/*******************************************************************************
*  函数名： OD_FlashWrite
*  输  入:  addr：写入地址，dat：写入数据，len：写入长度
*  输  出:  操作状态
*  功能说明： 向Flash中写入数据
*******************************************************************************/
uint8_t OD_FlashWrite(uint32_t addr, uint8_t *dat, uint16_t len)
{
  register uint16_t i;
  uint8_t flash_buf[OD_FLASH_PAGE_SIZE];
  FLASH_Status status;
 /*addr：基于基址的地址，addr_temp：实际物理地址，addr_page：基于基址的页地址，addr_offset：页的偏移地址*/
  uint32_t addr_temp, addr_page, addr_offset; 
  uint8_t erase_flag = 0;
  uint16_t data_temp = 0xFF00;
  if (addr >= OD_FLASH_MAX_SIZE)
  {
    DEBUG_WARNING("Flash addr is over the max size\r\n");
    return 1;
  }
  addr_page = (addr / OD_FLASH_PAGE_SIZE) * OD_FLASH_PAGE_SIZE;
  addr_offset = addr - addr_page;
  OD_FlashRead(addr_page, flash_buf, OD_FLASH_PAGE_SIZE);      /*读操作内部处理了基址*/
  for (i = addr_offset; i < (addr_offset + len); i++)
  {
    if (flash_buf[i] != 0xFF)
    {
      erase_flag = 1;
      break;
    }
  }
  
  if (erase_flag)  /*如果需要改写的地址无数据直接写入数据，如果有数据则需要将数据读出重新写*/
  {
    OD_FlashErasePage(addr_page, 1);                             /*擦除操作内部处理了基址*/
    memcpy((flash_buf + (addr - addr_page)), dat, len);          /*拷贝缓冲区的数据*/
    
    addr_temp = addr_page + OD_FLASH_ADDR_BASE;                  /*实际物理页地址*/
    FLASH_Unlock();  /*解锁Flash*/
    /*清除状态标志*/
    FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
    for (i = 0; i < OD_FLASH_PAGE_SIZE;)
    {
      status = FLASH_ProgramWord(addr_temp + i, *(uint32_t *)(flash_buf + i)); /*写入半字数据*/
      i += 4;
      if ((status == FLASH_ERROR_PG) || (status == FLASH_ERROR_WRP))
      {
        DEBUG_WARNING("Flash program error, error code is 0x%02x", status);
        return 1;
      }
    }
  }
  else
  {
    addr_temp = addr + OD_FLASH_ADDR_BASE;                  /*实际物理地址*/
    FLASH_Unlock();  /*解锁Flash*/
    /*清除状态标志*/
    FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
    for (i = 0; i < len;)
    {
      if ((i + 1) == len)
      {
        data_temp += *dat; 
        status = FLASH_ProgramHalfWord((addr_temp + i), data_temp);     
      }
      else
      {
        status = FLASH_ProgramHalfWord((addr_temp + i), *(uint16_t *)(dat + i));
      }
      i += 2;
      if ((status == FLASH_ERROR_PG) || (status == FLASH_ERROR_WRP))
      {
        DEBUG_WARNING("Flash program error, error code is 0x%02x", status);
        return 1;
      }
    }
  }
  
  FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
  
  FLASH_Lock();
  return 0;
}
