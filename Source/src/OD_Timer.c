/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Timer.c
* 内容简述: 定时器初始化设置程序
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.5.4  candy    创建文件
*
*******************************************************************************/
#include "OD_Timer.h"

/*******************************************************************************
*  函数名： od_TimerModeConfig
*  输  入:  无
*  输  出:  无
*  功能说明： 定时器模式初始化
*******************************************************************************/
static void od_TimerModeConfig(void)
{
  TIM_TimeBaseInitTypeDef  TIM_BaseInitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  TIM_BaseInitStructure.TIM_Prescaler = 719;                  /*时钟为72/(71+1)MHz*/
  TIM_BaseInitStructure.TIM_Period = 10000;                   /*100ms*/
  TIM_BaseInitStructure.TIM_ClockDivision = 0;
  TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_BaseInitStructure.TIM_RepetitionCounter = 0;
  
  TIM_TimeBaseInit(TIM2,&TIM_BaseInitStructure);   /*定时器2的控制模式初始化*/

  TIM_ARRPreloadConfig(TIM2, ENABLE);
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);      /*使能定时器中断*/
  TIM_Cmd(TIM2, ENABLE);
} 

/*******************************************************************************
*  函数名： OD_TimerInit
*  输  入:  无
*  输  出:  无
*  功能说明： 定时器初始化
*******************************************************************************/
void OD_TimerInit(void)
{
  od_TimerModeConfig();
}

