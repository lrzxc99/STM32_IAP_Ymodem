/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Device.c
* 内容简述: 设备信息程序
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.5.5  candy     创建文件
*
*******************************************************************************/
#include "OD_Device.h"
#include "common.h"
#include "OD_SystemTick.h"
#include <string.h>
#include <stdlib.h>

/*******************************************************************************
*  函数名： od_GetSerialNum
*  输  入:  无
*  输  出:  无
*  功能说明： 读取STM32唯一的96位标识码
* 
*******************************************************************************/
void od_GetSerialNum(void)
{
  uint32_t Device_Serial0, Device_Serial1, Device_Serial2;

  Device_Serial0 = *(uint32_t*)(0x1FFFF7E8);
  Device_Serial1 = *(uint32_t*)(0x1FFFF7EC);
  Device_Serial2 = *(uint32_t*)(0x1FFFF7F0);

  DEBUG_INFO("Serial number:\t%X-%X-%X\r\n\r\n", \
              Device_Serial0, Device_Serial1, Device_Serial2);
  
}

