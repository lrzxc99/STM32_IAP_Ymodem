/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: mian.c
* 内容简述: 欧帝BootLoader主程序
*
* 文件历史:
* 版本号    日期      作者      说明1
* v0.1    2014.11.3  candy    修改文件格式
*******************************************************************************/

#include "OD_Uart.h"
#include "common.h"
#include "OD_SystemTick.h"
#include "OD_Interrupt.h"
#include "OD_Timer.h"

/*******************************************************************************
*  函数名： main
*  输  入:  无
*  输  出:  无
*  功能说明： bootloader主程序
*******************************************************************************/
int main(void)
{
  FLASH_Unlock();
  OD_SystemTickInit();
  OD_UartInit();
  OD_BootInfoInit();
  OD_TimerInit();
  OD_InterruptInit();
  Main_Menu();
  while (1)
  {

  }
}
