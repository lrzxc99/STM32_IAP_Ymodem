/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_DebugPrint.c
* 内容简述: 调试信息的打印
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.5.20   candy     创建文件
* 
*******************************************************************************/
#include "OD_DebugPrint.h"
#include "OD_SystemTick.h"
#include "OD_Uart.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

static char buf[DEBUG_MAX_LEN];
#define LOGO_DISPLAY
/*******************************************************************************
*  函数名： DebugPrint
*  输  入:  level：调试等级，format：变参函数参数
*  输  出:  无
*  功能说明： 自定义调试打印函数
*******************************************************************************/
void DebugPrint(char *format, ...)
{
  va_list ap;          /* 定义变参函数 */


  va_start(ap, format);

  vsnprintf((char *)buf, DEBUG_MAX_LEN, format, ap);
  OD_UartWrite(USART1, (uint8_t *)buf, strlen(buf));

  free(buf);
  va_end(ap);
}


/*******************************************************************************
*  函数名： OD_BootInfoInit
*  输  入:  无
*  输  出:  无
*  功能说明： BootLoader信息的初始化
* 
*******************************************************************************/
void OD_BootInfoInit(void)
{
  OD_Delayms(100);          /*延时，避免串口出错*/

  DEBUG_INFO("**********************************************************************\r\n");
  DEBUG_INFO("*                    %s%s                 *\r\n", SOFT_NAME, CHIP);
  DEBUG_INFO("*                                                                    *\r\n");
  DEBUG_INFO("*                      BootLoader(Version:%s)                     *\r\n", SOFT_VERSION);
  DEBUG_INFO("*                                                                    *\r\n");
  DEBUG_INFO("*                   Nanjing Odin Technology Co., Ltd.                *\r\n");
  DEBUG_INFO("*                                                                    *\r\n");
  DEBUG_INFO("*                          www.njodin.com                            *\r\n");
  DEBUG_INFO("**********************************************************************\r\n");
  DEBUG_INFO("\r\n");
  
}

/*******************************************************************************
*  函数名： OD_LogoPrint
*  输  入:  无
*  输  出:  无
*  功能说明： 打印公司logo
* 
*******************************************************************************/
void OD_LogoPrint(void)
{
#ifdef LOGO_DISPLAY
  DEBUG_INFO("\r\n");
  DEBUG_INFO("              #####              ###      ###                     \r\n");
  DEBUG_INFO("            ########             ###      ###                     \r\n");
  DEBUG_INFO("            ###  ####            ###                              \r\n");
  DEBUG_INFO("           ####   ###            ###                              \r\n");
  DEBUG_INFO("           ####   ###            ###                              \r\n");  
  DEBUG_INFO("           ####   ###       ########      ###     ########        \r\n");
  DEBUG_INFO("           ####   ###      ###  ####      ###     #### ####       \r\n");
  DEBUG_INFO("           ####   ###     ####  ####      ###     ###   ###       \r\n");
  DEBUG_INFO("           ####   ###     ####   ###      ###     ###   ###       \r\n");
  DEBUG_INFO("           ####   ###     ####   ###      ###     ###   ###       \r\n");
  DEBUG_INFO("            ###   ###     ####  ####      ###     ###   ###       \r\n");
  DEBUG_INFO("            ###  ####      ###  ####      ###     ###   ###       \r\n");
  DEBUG_INFO("             #######       #########      ###     ###   ###       \r\n");
  DEBUG_INFO("              #####          #######      ###     ###   ###       \r\n");
  DEBUG_INFO("\r\n");
#endif
}


