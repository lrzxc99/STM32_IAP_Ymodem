/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Port.c
* 内容简述: 端口处理
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.4.22  candy    创建文件
*
*******************************************************************************/
#include "OD_GPIO.h"

/*******************************************************************************
*  函数名： od_GPIOPortConfig
*  输  入:  无
*  输  出:  无
*  功能说明： GPIO端口配置
* PA8=GPIO_OUT=GPIO0, PB13=GPIO_OUT=GPIO3, PA4=ADC0
* PB14=GPIO_OUT=GPIO2, PB15=GPIO_OUT=GPIO1
*******************************************************************************/
static void od_GPIOPortConfig(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5 | GPIO_Pin_6;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8 | GPIO_Pin_9;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);     
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);


  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0|GPIO_Pin_1 | GPIO_Pin_2;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOG, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOG, &GPIO_InitStructure);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);     
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 | GPIO_Pin_1 | GPIO_Pin_0 | GPIO_Pin_12;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOF, &GPIO_InitStructure);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; /*带上拉的输入*/
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOG, &GPIO_InitStructure);
  
//  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);    /*打开端口重映射*/
//  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE); /*使用SWD模式不使用JTAG*/
}

/*******************************************************************************
*  函数名： od_GPIOExtInterrupt
*  输  入:  无
*  输  出:  无
*  功能说明： GPIO的外部中断控制
*******************************************************************************/
void od_GPIOExtInterrupt(void)
{
  EXTI_InitTypeDef EXTI_InitStruct;

  EXTI_InitStruct.EXTI_Line = EXTI_Line4;  /*选择中断线路4，PA4*/
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt; /*选择中断模式*/
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling; /*选择下降沿中断*/
  EXTI_InitStruct.EXTI_LineCmd = ENABLE; /*使能线路*/
  
  EXTI_Init(&EXTI_InitStruct);
  EXTI_GenerateSWInterrupt(EXTI_Line4); /*使能中断*/
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource4); /*配置中断引脚，PA4*/
  EXTI_ClearITPendingBit(EXTI_Line4); /*清除中断*/
}

/*******************************************************************************
*  函数名： OD_GPIOInit
*  输  入:  无
*  输  出:  无
*  功能说明： GPIO初始化
*******************************************************************************/
void OD_GPIOInit(void)
{
  od_GPIOPortConfig();
  od_GPIOExtInterrupt();
}
