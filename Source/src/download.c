/**
  ******************************************************************************
  * @file    IAP/src/download.c 
  * @author  MCD Application Team
  * @version V3.3.0
  * @date    10/15/2010
  * @brief   This file provides the software which allows to download an image 
  *          to internal Flash.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
  */ 

/** @addtogroup IAP
  * @{
  */
/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "OD_SystemTick.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern uint8_t file_name[FILE_NAME_LENGTH];
uint8_t tab_1024[1024] =
  {
    0
  };

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Download a file via serial port
  * @param  None
  * @retval None
  */
void SerialDownload(void)
{
  int32_t Size = 0;

  DEBUG_INFO("Waiting for the file to be sent ... (press 'a' to abort)\n\r");
  Size = Ymodem_Receive(&tab_1024[0]);
  OD_Delayms(1000);
  if (Size > 0)
  {
    DEBUG_INFO("\r\n");
    DEBUG_INFO("Programming Completed Successfully!\r\n");
    DEBUG_INFO("--------------------------------\r\n");
    DEBUG_INFO("File name: %s\r\n", file_name);
    DEBUG_INFO("File size: %d bytes\r\n", Size);
    DEBUG_INFO("--------------------------------\r\n");
  }
  else if (Size == -1)
  {
    DEBUG_INFO("\r\nThe image size is higher than the allowed space memory!\r\n");
  }
  else if (Size == -2)
  {
    DEBUG_INFO("\r\nVerification failed!\r\n");
  }
  else if (Size == -3)
  {
    DEBUG_INFO("\r\nAborted by user.\r\n");
  }
  else
  {
    DEBUG_INFO("\r\nFailed to receive the file!\r\n");
  }
}

/**
  * @}
  */

/*******************(C)COPYRIGHT 2010 STMicroelectronics *****END OF FILE******/
