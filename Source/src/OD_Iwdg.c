/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_Iwdg.c
* 内容简述: 看门狗定时器
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.4.17  candy    创建文件
*
*******************************************************************************/
#include "OD_Iwdg.h"
#include "OD_Init.h"


/*******************************************************************************
*  函数名： OD_IwdgInit
*  输  入:  无
*  输  出:  无
*  功能说明： 看门狗初始化
*******************************************************************************/
void OD_IwdgInit(void)
{
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);

  IWDG_SetPrescaler(IWDG_Prescaler_64);     /*最大时间是reload=0xFFF时，时间为6553.6ms*/

  IWDG_SetReload(IWDG_RL_LOAD_VALUE);       /*设置超时*/     

  IWDG_ReloadCounter();

  IWDG_Enable();

  RCC_LSICmd(ENABLE); 
  OD_InitStatus.Iwdg = 1; /*设置Iwdg初始化状态*/
  if (OD_InitStatus.Uart == 1)
  {
    DEBUG_LOG("IWDG initialize ok\r\n");
  }
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY)==RESET);
}

/*******************************************************************************
*  函数名： OD_IwdgFeed
*  输  入:  无
*  输  出:  无
*  功能说明： 看门狗喂狗
*******************************************************************************/
void OD_IwdgFeed(void)
{
  /* 喂狗操作*/
  IWDG_ReloadCounter();
}

