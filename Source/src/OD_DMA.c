/*******************************************************************************
* Copyright (C), 2014-2015, www.njodin.com
*
* 文件名: OD_DMA.c
* 内容简述: 使用DMA控制器
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2014.4.29  candy    创建文件
* 
*******************************************************************************/
#include "OD_DMA.h"
#include "stm32f10x_dma.h"
#include "OD_Init.h"
#include "OD_Uart.h"

extern volatile uint8_t DMA_Buff[OD_UART_TX_BUF_SIZE];

/*******************************************************************************
*  函数名： OD_DMAInit
*  输  入:  无
*  输  出:  无
*  功能说明： DMA模式配置
*******************************************************************************/
void OD_DMAInit(void)
{
  DMA_InitTypeDef DMA_InitStructure;
  
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  
  DMA_DeInit(DMA1_Channel2);                              /*串口3发送*/
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART3->DR;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_Buff;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = 0;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel2, &DMA_InitStructure);
  DMA_ITConfig(DMA1_Channel2, DMA_IT_TC,ENABLE);
  
  DMA_DeInit(DMA1_Channel4);                              /*串口1发送*/
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_Buff;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = 0;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel4, &DMA_InitStructure);
  DMA_ITConfig(DMA1_Channel4, DMA_IT_TC,ENABLE);
  
  DMA_DeInit(DMA1_Channel7);                              /*串口2发送*/
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART2->DR;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_Buff;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = 0;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel7, &DMA_InitStructure);
  DMA_ITConfig(DMA1_Channel7, DMA_IT_TC,ENABLE);
  
  
  OD_InitStatus.DMA = 1;  /*设置DMA初始化状态*/
  if (OD_InitStatus.Uart == 1)
  {
    DEBUG_LOG("DMA initialize ok\r\n");
  }
}

