#ifndef __OD_IWDG_H__

#define __OD_IWDG_H__
#include "global.h"

#define IWDG_RL_MAX_VALUE   0xFFF     /*RL的最大值为4095*/    
#define IWDG_MAX_TIME       6554      /*时间最长为6554ms*/
#define IWDG_SET_TIME       5000      /*时间超时值为5000ms*/

#define IWDG_RL_LOAD_VALUE  (IWDG_RL_MAX_VALUE * IWDG_SET_TIME / IWDG_MAX_TIME)

extern void OD_IwdgInit(void);
extern void OD_IwdgFeed(void);

#endif  /*__OD_IWDG_H__*/
