#ifndef __OD_UART_H__
#define __OD_UART_H__

#include "global.h"
#include "OD_GPIO.h"
#include "OD_CircularQueue.h"

#define OD_UART1_USED   1
#define OD_UART2_USED   0
#define OD_UART3_USED   0

typedef struct UART_BUFF_STR
{
  USART_TypeDef* USARTx;
  DMA_Channel_TypeDef *DMA_TX;
}UartBuff_Str;


#if OD_UART1_USED
#define OD_UART1_BAUD         115200 
#define OD_UART1_WORD_LEN     USART_WordLength_8b
#define OD_UART1_STOPBIT      USART_StopBits_1
#define OD_UART1_PARITY       USART_Parity_No
#define OD_UART1_BUFF_SIZE    512
extern volatile UartBuff_Str OD_Uart1;
#endif /*OD_UART1_USED*/

#if OD_UART2_USED
#define OD_UART2_BAUD         9600 
#define OD_UART2_WORD_LEN     USART_WordLength_8b
#define OD_UART2_STOPBIT      USART_StopBits_1
#define OD_UART2_PARITY       USART_Parity_No
#define OD_UART2_BUFF_SIZE    64
extern volatile UartBuff_Str OD_Uart2;
#endif /*OD_UART2_USED*/

#if OD_UART3_USED
#define OD_UART3_BAUD         9600 
#define OD_UART3_WORD_LEN     USART_WordLength_8b
#define OD_UART3_STOPBIT      USART_StopBits_1
#define OD_UART3_PARITY       USART_Parity_No
#define OD_UART3_BUFF_SIZE    64
extern volatile UartBuff_Str OD_Uart3;
#endif /*OD_UART2_USED*/

extern void OD_UartInit(void);
extern void OD_UartWrite(USART_TypeDef *USARTx, uint8_t *wdata, uint16_t len);

#endif /*__OD_UART_H__*/
