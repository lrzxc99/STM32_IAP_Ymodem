#ifndef __OD_FLASH_H__
#define __OD_FLASH_H__
#include "global.h"

#if defined STM32F10X_MD
 #define OD_FLASH_PAGE_SIZE                         (0x400)    /* 1 Kbyte */
 #define OD_FLASH_MAX_SIZE                          (0x20000)  /* 128 KBytes */
#elif defined STM32F10X_CL
 #define OD_FLASH_PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define OD_FLASH_MAX_SIZE                          (0x40000)  /* 256 KBytes */
#elif defined STM32F10X_HD
 #define OD_FLASH_PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define OD_FLASH_MAX_SIZE                          (0x80000)  /* 512 KBytes */
#elif defined STM32F10X_XL
 #define OD_FLASH_PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define OD_FLASH_MAX_SIZE                          (0x100000)  /* 1024 KBytes */
#else 
 #error "Please select first the STM32 device to be used (in stm32f10x.h)"    
#endif

#define OD_FLASH_ADDR_BASE                          (0x08000000)    /*数据存储空间的基址*/

extern void OD_FlashInit(void);
extern uint8_t OD_FlashErasePage(uint32_t page, uint16_t len);
extern uint8_t OD_FlashWrite(uint32_t addr, uint8_t *dat, uint16_t len);
extern void OD_FlashRead(uint32_t addr, uint8_t *dat, uint16_t len);
#endif /*__OD_FLASH_H__*/
