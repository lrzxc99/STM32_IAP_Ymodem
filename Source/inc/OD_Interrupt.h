
#ifndef __OD_INTERRUPT_H
#define __OD_INTERRUPT_H
#include "Global.h"

extern void NMI_Handler(void);
extern void HardFault_Handler(void);
extern void MemManage_Handler(void);
extern void BusFault_Handler(void);
extern void UsageFault_Handler(void);
extern void SVC_Handler(void);
extern void DebugMon_Handler(void);
extern void PendSV_Handler(void);
extern void SysTick_Handler(void);
extern void RTC_IRQHandler(void);
extern void SDIO_IRQHandler(void);
extern void USART1_IRQHandler(void);
extern void EXTI9_5_IRQHandler(void);

extern void OD_InterruptInit(void);

#endif /*__OD_INTERRUPT_H*/


