#ifndef __OD_DEVICE_H__
#define __OD_DEVICE_H__
#include "global.h"
#include "OD_DebugPrint.h"

#define SOFT_NAME              "Odin BootLoader for "
#define SOFT_VERSION           "1.0.2" 
#define SOFT_MAKE_TIME         __DATE__

#if defined (STM32F10X_MD)
#define CHIP                    "STM32F103C8"
#define FLASH_MAX_SIZE          0x10000

#elif defined (STM32F10X_HD) 
#define CHIP                    "STM32F103ZE"
#define FLASH_MAX_SIZE          0x80000

#elif defined (STM32F10X_CL)
#define CHIP                    "STM32F105RC"
#define FLASH_MAX_SIZE          0x40000
#endif

extern void OD_BootInfoInit(void);
extern void OD_LogoPrint(void);
#endif /*__DEVICE_H__*/
