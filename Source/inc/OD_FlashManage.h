#ifndef __OD_FLASHMANAGE_H__
#define __OD_FLASHMANAGE_H__
#include "global.h"
#include "OD_Flash.h"

#define FM_MAX_SIZE             OD_FLASH_MAX_SIZE          /*FM管理的总空间大小为128K*/
#define FM_PAGE_SIZE            OD_FLASH_PAGE_SIZE          /*页的大小为2K*/
#define FM_PAGE_NUM             (FM_MAX_SIZE / FM_PAGE_SIZE)

#define FM_INF_ADDR_BASE        0x00
#define FM_INF_PAGE             0x00                   /*FM数据管理系统的信息，保存在第0页开始位置，大小占1页*/
#define FM_INF_NAME             "FMT2.3_SPLIT"         /*数据管理系统名称，Flash Manage Tang*/
#define FM_INF_NAME_SIZE        sizeof(FM_INF_NAME)    /*FMT数据管理系统的名称长度*/
#define FM_INF_VISION           0x02                   /*系统版本，占1字节*/
#define FM_INF_MAX_SIZE         FM_MAX_SIZE            /*最大字节数，占4字节*/
#define FM_INF_PAGE_SIZE        FM_PAGE_SIZE           /*每页字节数据，占4字节*/
#define FM_INF_SEC_SIZE         FM_SEC_TOTAL_SIZE      /*扇区字节数, 占4字节*/

#define FM_INF_SIZE             (FM_INF_NAME_SIZE+13) /*FM管理的初始化信息的信息长度*/

/*扇区配置信息*/
#define FM_SEC_ADDR_BASE        FM_PAGE_SIZE      
#define FM_SEC_INF_SIZE         4       /*扇区信息字节大小*/         
#define FM_SEC_DAT_SIZE         (FM_PAGE_SIZE - FM_SEC_INF_SIZE)    /*扇区大小为2044字节*/
#define FM_SEC_TOTAL_SIZE       (FM_SEC_INF_SIZE + FM_SEC_DAT_SIZE) /*扇区总共占用字节数*/
#define FM_SEC_NUM              ((FM_MAX_SIZE - FM_SEC_ADDR_BASE) / FM_INF_SEC_SIZE)
/*扇区信息，占4字节。第1，2字节为扇区编号标志信息，第3，4字节为扇区号*/
#define FM_SEC_HEAD_SIZE        2       /*扇区头占用的位数,因为STM32一次只能写入2字节。不能写入1字节*/ 
#define FM_SEC_ID_SIZE          2       /*扇区ID号占用的位数*/
#define FM_SEC_FREE             0xFFFF  /*扇区为空*/
#define FM_SEC_VALID            0xC0C0  /*扇区数据有效*/
#define FM_SEC_NOUSE            0xE0E0  /*扇区数据无效*/
#define FM_SEC_DIRTY            0x8080  /*扇区数据可擦除*/

#define FM_SEC_ID_MASK          0xFFFF
#define FM_SEC_HEAD_MASK        0xFFFF
#define FM_SEC_INF_ID_MASK      0xFFFF0000
#define FM_SEC_INF_HEAD_MASK    0x0000FFFF

#define FM_SEC_INF_ERR          0xFFFFFFFF
#define FM_SEC_ID_ERR           0xFFFF


/*存储的数据名称从扇区第2字节存储，第0，第1字节存储扇区头*/
#define DATA_NAME_OFFSET        0x04                                                                                
#define DATA_NAME_SIZE          0x0A                                  /*数据名称最大长度为10字节*/                  
#define DATA_ID_OFFSET          (DATA_NAME_OFFSET + DATA_NAME_SIZE)   /*数据Id号偏移地址*/                          
#define DATA_ID_SIZE            0x02                                  /*数据Id号占1字节*/                           
#define DATA_LEN_OFFSET         (DATA_ID_OFFSET + DATA_ID_SIZE)       /*数据长度偏移地址*/                          
#define DATA_LEN_SIZE           0x02                                  /*数据长度占1字节，如果写满扇区*/             
#define DATA_DAT_OFFSET         (DATA_LEN_OFFSET + DATA_LEN_SIZE)     /*数据存储的偏移地址*/                        
                                                                                                                    
#define DATA_DATA_MAX_LEN       (FM_SEC_TOTAL_SIZE - DATA_DAT_OFFSET)                                               
                                                                                                                    
#define DATA_ID_START           0x0000                                /*数据编号从0开始*/                           
#define DATA_ID_END             0xFFFF                                /*数据编号结束为0xFF*/                        

/*FMT数据管理系统的扇区头类型*/
typedef enum FMSecHead_ENU
{
  FM_FREE = 0,
  FM_VALID,
  FM_NOUSE,
  FM_DIRTY
}FMSecHead_Enu;

#pragma pack(push) /*保存先前的对齐规则*/
#pragma pack(2)    /*强制1字节对齐*/
/*FM数据管理的系统信息*/
typedef struct FMINF_STR
{
  uint8_t name[FM_INF_NAME_SIZE];
  uint8_t Vision;
  uint32_t MaxSize;
  uint32_t PageSize;
  uint32_t SECSize;
}FMINF_Str;


/*FM数据管理的系统数据存储的格式信息*/
typedef struct FMData_STR
{
  uint8_t name[DATA_NAME_SIZE];    /*数据文件的名称*/
  uint16_t id;                     /*数据文件的编号，编号从0开始*/
  uint16_t len;                    /*数据文件的长度*/
}FMData_Str;
#pragma pack(pop) /*恢复先前的对齐规则*/

#ifdef FM_STRUCT_DEF
volatile FMINF_Str FMInfo;
#undef FM_STRUCT_DEF
#else /*FM_STRUCT_DEF*/
extern volatile FMINF_Str FMInfo;
#endif /*FM_STRUCT_DEF*/

extern void OD_FlashManageInit(void);
extern void OD_FMFormat(void);
extern void OD_FlashManageWrite(uint8_t *name, uint8_t *dat, uint16_t len);
extern uint8_t OD_FlashManageRead(uint8_t *name, uint8_t *dat);
extern void OD_FlashManageDelete(uint8_t *name);
#endif /*__OD_FLASHMANAGE_H__*/
