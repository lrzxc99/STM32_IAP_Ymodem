#ifndef __OD_CIRCULARQUEUE_H__
#define __OD_CIRCULARQUEUE_H__
#include "global.h"

typedef struct CircularQueue_STR
{
  uint8_t   *BuffHead;    //缓冲区头
  uint16_t  WritePtr;     //写入指针index
  uint16_t  ReadPtr;      //读取指针index
  uint16_t  Count;        //可用数据长度
  uint16_t  BuffSize;     //最大缓冲区大小
}CirQueue_Str;

extern void OD_QueueInit(CirQueue_Str *queue, uint8_t *bufHead, uint16_t bufSize);
extern uint8_t OD_QueueEmpty(CirQueue_Str *queue);
extern uint8_t OD_QueueFull(CirQueue_Str *queue);
extern void OD_QueueClear(CirQueue_Str *queue);
extern void OD_EnQueue(CirQueue_Str *queue, uint8_t dat);
extern uint16_t OD_DeQueue(CirQueue_Str *queue);
#endif  /*__OD_CIRCULARQUEUE_H__*/
