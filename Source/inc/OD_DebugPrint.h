#ifndef __OD_DEBUGPRINT_H__
#define __OD_DEBUGPRINT_H__
#include <stdio.h>


#define SOFT_NAME              "Odin BootLoader for "
#define SOFT_VERSION           "1.1.0" 
#define SOFT_MAKE_TIME         __DATE__

#if defined (STM32F10X_MD)
#define CHIP                    "STM32F103C8"
#define FLASH_MAX_SIZE          0x10000

#elif defined (STM32F10X_HD) 
#define CHIP                    "STM32F103ZE"
#define FLASH_MAX_SIZE          0x80000

#elif defined (STM32F10X_CL)
#define CHIP                    "STM32F105RC"
#define FLASH_MAX_SIZE          0x40000

#elif defined (STM32F10X_XL)
#define CHIP                    "STM32F103ZG"
#define FLASH_MAX_SIZE          0x100000
#endif

/* 定义调试信息最大长度 */
#define DEBUG_MAX_LEN         250

#define OD_DEBUG_EN           0

#define OD_DEBUG_INFO         1
#define OD_DEBUG_LOG          1
#define OD_DEBUG_WARNING      1
#define OD_DEBUG_ERROR        1

extern void DebugPrint(char *format, ...);

#if OD_DEBUG_INFO
#define DEBUG_INFO(format, ...)     DebugPrint(format, ##__VA_ARGS__)
#else
#define DEBUG_INFO(format, ...)
#endif

#if OD_DEBUG_LOG
#define DEBUG_LOG(format, ...)      DebugPrint(format, ##__VA_ARGS__)
#else
#define DEBUG_LOG(format, ...)
#endif

#if OD_DEBUG_WARNING
#define DEBUG_WARNING(format, ...)  DebugPrint(format, ##__VA_ARGS__)
#else
#define DEBUG_WARNING(format, ...)
#endif

#if OD_DEBUG_ERROR
#define DEBUG_ERROR(format, ...)    DebugPrint("Function->%s,Line->%d:"format,   __FUNCTION__, __LINE__,  ##__VA_ARGS__)
#else
#define DEBUG_ERROR(format, ...)
#endif


extern void OD_BootInfoInit(void);
extern void OD_LogoPrint(void);
#endif
