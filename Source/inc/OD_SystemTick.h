
#ifndef __OD_SYSTEMTICK_H__
#define __OD_SYSTEMTICK_H__
#include "global.h"

extern void OD_Delayms(uint16_t ms);
extern void OD_Delay1us(uint16_t us);
extern void OD_SystemTickInit(void);
#endif /*__OD_SYSTEMTICK_H__*/
